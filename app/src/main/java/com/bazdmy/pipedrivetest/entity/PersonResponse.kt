package com.bazdmy.pipedrivetest.entity

import com.google.gson.annotations.SerializedName

data class RemoteResponse<T>(
        @SerializedName("success") var success: Boolean? = false,
        @SerializedName("error") var error: String? = null,
        @SerializedName("error_info") var errorInfo: String? = null,
        @SerializedName("data") var data: T?=null,
        @SerializedName("additional_data") var additionalData: AdditionalData? = AdditionalData()

)