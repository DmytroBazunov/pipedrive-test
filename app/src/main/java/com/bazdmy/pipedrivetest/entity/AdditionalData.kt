package com.bazdmy.pipedrivetest.entity

import com.google.gson.annotations.SerializedName

data class AdditionalData(
        @SerializedName("pagination") var pagination: Pagination? = Pagination()
)