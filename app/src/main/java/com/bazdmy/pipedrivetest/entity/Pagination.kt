package com.bazdmy.pipedrivetest.entity

import com.google.gson.annotations.SerializedName

data class Pagination(
        @SerializedName("start") var start: Int? = 0,
        @SerializedName("limit") var limit: Int? = 0,
        @SerializedName("more_items_in_collection") var moreItemsInCollection: Boolean? = false
)