package com.bazdmy.pipedrivetest.entity

class RemoteException(message: String) : Exception(message)