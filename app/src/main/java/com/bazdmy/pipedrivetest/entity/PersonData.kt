package com.bazdmy.pipedrivetest.entity

import com.bazdmy.pipedrivetest.database.Email
import com.bazdmy.pipedrivetest.database.Organization
import com.bazdmy.pipedrivetest.database.Person
import com.bazdmy.pipedrivetest.database.Phone
import com.google.gson.annotations.SerializedName


class PersonData(
        @SerializedName("org_id")
        var organization: Organization? = Organization(),
        @SerializedName("phone")
        var phone: List<Phone>? = listOf(),
        @SerializedName("email")
        var email: List<Email>? = listOf()
) : Person()