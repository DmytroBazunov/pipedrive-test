package com.bazdmy.pipedrivetest.entity

import com.google.gson.annotations.SerializedName

data class Pictures(
        @SerializedName("128") var x128: String? = "",
        @SerializedName("512") var x512: String? = ""
)