package com.bazdmy.pipedrivetest.di

import com.bazdmy.pipedrivetest.persondetail.PersonDetailFragmet
import com.bazdmy.pipedrivetest.persons.PersonsListFragment
import com.bazdmy.pipedrivetest.persons.PersonsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun bindPersonsActivity(): PersonsActivity

    @ContributesAndroidInjector
    abstract fun bindPersonDetailFragment(): PersonDetailFragmet

    @ContributesAndroidInjector
    abstract fun bindPersonsListFragment(): PersonsListFragment
}
