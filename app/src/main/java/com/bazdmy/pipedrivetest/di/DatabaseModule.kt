package com.bazdmy.pipedrivetest.di

import android.arch.persistence.room.Room
import android.content.Context
import com.bazdmy.pipedrivetest.database.AppDatabase
import com.bazdmy.pipedrivetest.database.PersonDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun provideDB(context: Context): AppDatabase {
        return Room.databaseBuilder(context,
                AppDatabase::class.java, "pipedrive-name").build()
    }

    @Provides
    @Singleton
    fun providePersonDao(db: AppDatabase): PersonDao {
        return db.personDao()
    }
}