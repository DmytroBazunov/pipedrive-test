package com.bazdmy.pipedrivetest.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.bazdmy.pipedrivetest.R
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Singleton
    @Provides
    fun provideContext(application: Application): Context = application

    @Singleton
    @Provides
    fun provideApiKey(application: Context): String = application.getString(R.string.api_token)

    @Singleton
    @Provides
    fun provideShared(context: Context): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
}