package com.bazdmy.pipedrivetest.di

import android.app.Application
import com.bazdmy.pipedrivetest.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidSupportInjectionModule::class), (ApplicationModule::class), (ActivityBuilder::class), RemoteModule::class, DatabaseModule::class])
interface ApplicationComponent {
    fun inject(application: App)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder

        fun build(): ApplicationComponent
    }
}