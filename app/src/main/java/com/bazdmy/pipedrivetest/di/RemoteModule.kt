package com.bazdmy.pipedrivetest.di

import android.content.Context
import com.bazdmy.pipedrivetest.R
import com.bazdmy.pipedrivetest.persons.PersonsApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
class RemoteModule {

    @Singleton
    @Provides
    fun provideOkhttp(): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor {
                    val request: Request = it.request()
                    val builder = request.newBuilder()
                    return@addInterceptor it.proceed(builder.build())
                }
                .addInterceptor(HttpLoggingInterceptor().also { it.level = HttpLoggingInterceptor.Level.BODY })
                .build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(okHttp: OkHttpClient, context: Context): Retrofit {
        return Retrofit.Builder()
                .baseUrl(context.getString(R.string.URL))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttp)
                .build()
    }

    @Singleton
    @Provides
    fun providePersonsAPI(retrofit: Retrofit): PersonsApi {
        return retrofit.create(PersonsApi::class.java)
    }

}