package com.bazdmy.pipedrivetest.persondetail


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.bazdmy.pipedrivetest.R
import com.bazdmy.pipedrivetest.database.PersonInfo
import com.squareup.picasso.Picasso
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_person_detail_fragmet.*
import javax.inject.Inject


class PersonDetailFragmet : Fragment() {

    @Inject
    lateinit var viewModelDetailFactory: PersonDetailViewModelFactory

    private lateinit var viewModel: PersonDetailViewModel

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        viewModel = ViewModelProviders.of(context as FragmentActivity, viewModelDetailFactory)
                .get(PersonDetailViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_person_detail_fragmet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.person.observe(this,
                Observer<PersonInfo> { it ->
                    if (it == null) return@Observer
                    person_name_tv.text = it.person.name
                    person_org_tv.text = it.organization.name
                    val gravatrURL = it.getGravatarURL(512)
                    Picasso.get()
                            .load(gravatrURL)
                            .into(person_ava_iv)
                    if (it.emails.any { !it.value.isEmpty() }) {
                        email_rv.visibility = View.VISIBLE
                        mail_iv.visibility = View.VISIBLE
                        email_rv.apply {
                            layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
                            adapter = EmailAdapter(it.emails)
                        }
                    } else {
                        email_rv.visibility = View.GONE
                        mail_iv.visibility = View.GONE
                    }
                    if (it.phones.any { !it.value.isEmpty() }) {
                        phone_rv.visibility = View.VISIBLE
                        phone_iv.visibility = View.VISIBLE
                        phone_rv.apply {
                            layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
                            adapter = PhoneAdapter(it.phones.filter { !it.value.isEmpty() })
                        }
                    } else {
                        phone_rv.visibility = View.GONE
                        phone_iv.visibility = View.GONE
                    }
                }
        )
    }

}
