package com.bazdmy.pipedrivetest.persondetail

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bazdmy.pipedrivetest.R
import com.bazdmy.pipedrivetest.database.Email
import com.bazdmy.pipedrivetest.database.Phone
import kotlinx.android.synthetic.main.item_phone.view.*

class EmailAdapter(private var emails: List<Email>) : RecyclerView.Adapter<EmailAdapter.EmailViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): EmailViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_phone, parent, false)
        return EmailViewHolder(view)
    }

    override fun getItemCount(): Int =
            emails.size


    override fun onBindViewHolder(holder: EmailViewHolder, position: Int) {
        holder.bind(emails[position])
    }

    class EmailViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(email: Email) {
            itemView.phone_tv.text = email.value
            itemView.status_tv.text = email.label
        }
    }
}

class PhoneAdapter(private var phones: List<Phone>) : RecyclerView.Adapter<PhoneAdapter.PhoneViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): PhoneViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_phone, parent, false)
        return PhoneViewHolder(view)
    }

    override fun getItemCount(): Int =
            phones.size


    override fun onBindViewHolder(holder: PhoneViewHolder, position: Int) {
        holder.bind(phones[position])
    }

    class PhoneViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(phone: Phone) {
            itemView.phone_tv.text = phone.value
            itemView.status_tv.text = phone.label
        }
    }
}
