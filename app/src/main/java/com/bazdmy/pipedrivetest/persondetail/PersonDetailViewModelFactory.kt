package com.bazdmy.pipedrivetest.persondetail

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bazdmy.pipedrivetest.persons.PersonRepository
import javax.inject.Inject

class PersonDetailViewModelFactory @Inject
constructor(private val personRepository: PersonRepository
            )
    : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PersonDetailViewModel(personRepository) as T
    }

}