package com.bazdmy.pipedrivetest.persondetail

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.bazdmy.pipedrivetest.database.PersonInfo
import com.bazdmy.pipedrivetest.persons.PersonRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

class PersonDetailViewModel constructor(private val personRepo: PersonRepository
) : ViewModel() {

    var person: LiveData<PersonInfo>
    var error: MutableLiveData<String> = MutableLiveData()
    private val compositDisposable = CompositeDisposable()

    var personIdLD: MutableLiveData<Int> = MutableLiveData()

    init {
        personIdLD.postValue(-1)
        person = Transformations.switchMap(personIdLD) {
            personRepo.getPersonById(it)
        }
    }

    fun setPersonId(personId: Int) {
        personIdLD.postValue(personId)
        personRepo.syncPerson(personId)
                .subscribe({}, {
                    error.postValue("No Internet connection")
                })
                .addTo(compositDisposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositDisposable.clear()
    }


}