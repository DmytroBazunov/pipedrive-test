package com.bazdmy.pipedrivetest.persons

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.bazdmy.pipedrivetest.R
import com.bazdmy.pipedrivetest.persondetail.PersonDetailFragmet
import com.bazdmy.pipedrivetest.persondetail.PersonDetailViewModel
import com.bazdmy.pipedrivetest.persondetail.PersonDetailViewModelFactory
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_person.*
import javax.inject.Inject


class PersonsActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = fragmentDispatchingAndroidInjector

    @Inject
    lateinit var viewModelFactory: PersonsViewModelFactory
    @Inject
    lateinit var viewModelDetailFactory: PersonDetailViewModelFactory
    private var isTwoPane: Boolean = false
    private lateinit var viewModel: PersonsListViewModel
    private lateinit var viewModelDetail: PersonDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_person)
        setSupportActionBar(toolbar)
        isTwoPane = findViewById<View>(R.id.detail) != null
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(PersonsListViewModel::class.java)
        viewModelDetail = ViewModelProviders.of(this, viewModelDetailFactory)
                .get(PersonDetailViewModel::class.java)

        viewModelDetail.personIdLD.observe(this, Observer<Int> { id ->
            if (id ?: -1 < 0) {
                showList()
            } else {
                showDetail()
            }
        })
    }

    private fun showDetail() {
        if (!isTwoPane) {
            val fragmentDetail = supportFragmentManager.findFragmentByTag("detail")
                    ?: PersonDetailFragmet()
            reatachFragmentToDifferentContainer(R.id.list_frag, fragmentDetail, "detail")
        } else {
            val fragmentDetail = supportFragmentManager.findFragmentByTag("detail")
                    ?: PersonDetailFragmet()
            if (!fragmentDetail.isVisible)
                reatachFragmentToDifferentContainer(R.id.detail, fragmentDetail, "detail")
            detail?.visibility = View.VISIBLE
            val fragment = supportFragmentManager.findFragmentByTag("list_fragment")
                    ?: PersonsListFragment()
            supportFragmentManager.beginTransaction()
                    .replace(R.id.list_frag, fragment, "list_fragment")
                    .commit()

        }
    }

    private fun showList() {
        if (!isTwoPane) {
            val fragment = supportFragmentManager.findFragmentByTag("list_fragment")
                    ?: PersonsListFragment()
            supportFragmentManager.beginTransaction()
                    .replace(R.id.list_frag, fragment, "list_fragment")
                    .commit()
        } else {
            val fragment = supportFragmentManager.findFragmentByTag("list_fragment")
                    ?: PersonsListFragment()
            detail?.visibility = View.GONE
            supportFragmentManager.beginTransaction()
                    .replace(R.id.list_frag, fragment, "list_fragment")

                    .commit()
        }
    }

    override fun onBackPressed() {
        viewModelDetail.setPersonId(-1)
        super.onBackPressed()
    }


    private fun reatachFragmentToDifferentContainer(id: Int, fragment: Fragment, tag: String) {
        supportFragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        supportFragmentManager.beginTransaction().remove(fragment).commit()
        supportFragmentManager.executePendingTransactions()
        supportFragmentManager.beginTransaction()
                .replace(id, fragment, tag)
                .addToBackStack(null)
                .commit()
    }
}