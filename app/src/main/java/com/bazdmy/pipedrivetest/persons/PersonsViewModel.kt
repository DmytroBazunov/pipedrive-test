package com.bazdmy.pipedrivetest.persons

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.bazdmy.pipedrivetest.database.PersonWithEmail
import io.reactivex.disposables.CompositeDisposable

class PersonsListViewModel constructor(
        private val personRepo: PersonRepository) : ViewModel() {

    var personsList: LiveData<PagedList<PersonWithEmail>>
    private val compositeDisposable = CompositeDisposable()
    private val pageSize = 100
    private val personsDataSourceFactory: PersonsDataSourceFactory

    init {
        personsDataSourceFactory = PersonsDataSourceFactory(compositeDisposable, personRepo)
        val config = PagedList.Config.Builder()
                .setPageSize(pageSize)
                .setInitialLoadSizeHint(pageSize * 2)
                .setEnablePlaceholders(false)
                .build()
        personsList = LivePagedListBuilder<Int, PersonWithEmail>(personsDataSourceFactory, config).build()
    }


    fun getState(): LiveData<Pair<State, String>> = Transformations.switchMap<PersonsDataSource,
            Pair<State, String>>(personsDataSourceFactory.personsDataSourceLiveData, PersonsDataSource::state)

    fun retry() {
        personsDataSourceFactory.personsDataSourceLiveData.value?.retry()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}