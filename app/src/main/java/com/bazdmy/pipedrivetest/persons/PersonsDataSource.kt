package com.bazdmy.pipedrivetest.persons

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import com.bazdmy.pipedrivetest.database.PersonWithEmail
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers

class PersonsDataSource(
        private val personRepo: PersonRepository,
        private val compositeDisposable: CompositeDisposable
) : PageKeyedDataSource<Int, PersonWithEmail>() {

    var state: MutableLiveData<Pair<State, String>> = MutableLiveData()
    private var retryCompletable: Completable? = null


    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, PersonWithEmail>) {
        updateState(State.LOADING to "")
        compositeDisposable.add(
                personRepo.getPersonsWithOffset(0, params.requestedLoadSize,::updateState)
                        .subscribe(
                                { data ->
                                    updateState(State.DONE to "")
                                    callback.onResult(data,
                                            null,
                                            data.size
                                    )
                                },
                                {
                                    updateState(State.ERROR to "")
                                    setRetry(Action { loadInitial(params, callback) })
                                }
                        )
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, PersonWithEmail>) {
        updateState(State.LOADING to "")
        compositeDisposable.add(
                personRepo.getPersonsWithOffset(params.key, params.requestedLoadSize, ::updateState)
                        .subscribe(
                                { data ->
                                    updateState(State.DONE to "")
                                    callback.onResult(data,
                                            params.key + data.size
                                    )
                                },
                                {
                                    updateState(State.ERROR to it.localizedMessage)
                                    setRetry(Action { loadAfter(params, callback) })
                                }
                        )
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, PersonWithEmail>) {
    }

    private fun updateState(state: Pair<State, String>) {
        this.state.postValue(state)
    }

    fun retry() {
        if (retryCompletable != null) {
            compositeDisposable.add(retryCompletable!!
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe())
        }
    }

    private fun setRetry(action: Action?) {
        retryCompletable = if (action == null) null else Completable.fromAction(action)
    }

}

enum class State {
    DONE, LOADING, ERROR
}