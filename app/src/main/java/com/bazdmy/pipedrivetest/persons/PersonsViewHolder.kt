package com.bazdmy.pipedrivetest.persons

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bazdmy.pipedrivetest.R
import com.bazdmy.pipedrivetest.database.PersonWithEmail
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_person.view.*


class PersonsListAdapter(private val onClick: (PersonWithEmail?) -> Unit)
    : PagedListAdapter<PersonWithEmail, RecyclerView.ViewHolder>(NewsDiffCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PersonsViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PersonsViewHolder).bind(getItem(position))
        holder.itemView.setOnClickListener {
            onClick.invoke(getItem(position))
        }
    }


    companion object {
        val NewsDiffCallback = object : DiffUtil.ItemCallback<PersonWithEmail>() {
            override fun areItemsTheSame(oldItem: PersonWithEmail, newItem: PersonWithEmail): Boolean {
                return oldItem.person.id == newItem.person.id
            }

            override fun areContentsTheSame(oldItem: PersonWithEmail, newItem: PersonWithEmail): Boolean {
                return oldItem == newItem
            }
        }
    }

}

class PersonsViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(person: PersonWithEmail?) {
        if (person != null) {
            itemView.name_tv.text = person.person.name
            val gravatrURL = person.getGravatarURL(128)

            Picasso.get()
                    .load(gravatrURL)
                    .into(itemView.avatar_iv)
        }
    }

    companion object {
        fun create(parent: ViewGroup): PersonsViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_person, parent, false)
            return PersonsViewHolder(view)
        }
    }
}