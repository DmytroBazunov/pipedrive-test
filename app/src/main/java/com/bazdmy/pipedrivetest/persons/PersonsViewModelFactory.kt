package com.bazdmy.pipedrivetest.persons

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import javax.inject.Inject

class PersonsViewModelFactory @Inject constructor(private val repository: PersonRepository) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PersonsListViewModel(repository) as T
    }

}