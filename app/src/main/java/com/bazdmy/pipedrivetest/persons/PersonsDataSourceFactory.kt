package com.bazdmy.pipedrivetest.persons


import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.bazdmy.pipedrivetest.database.PersonWithEmail
import io.reactivex.disposables.CompositeDisposable

class PersonsDataSourceFactory(
        private val compositeDisposable: CompositeDisposable,
        private val personRepo: PersonRepository)
    : DataSource.Factory<Int, PersonWithEmail>() {

    val personsDataSourceLiveData = MutableLiveData<PersonsDataSource>()

    override fun create(): DataSource<Int, PersonWithEmail> {
        val personsDataSource = PersonsDataSource(personRepo, compositeDisposable)
        personsDataSourceLiveData.postValue(personsDataSource)
        return personsDataSource
    }
}