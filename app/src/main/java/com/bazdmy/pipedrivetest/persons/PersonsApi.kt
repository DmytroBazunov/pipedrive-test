package com.bazdmy.pipedrivetest.persons

import com.bazdmy.pipedrivetest.entity.PersonData
import com.bazdmy.pipedrivetest.entity.RemoteResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PersonsApi {

    @GET("v1/persons")
    fun getAllPersons(@Query("start") start: Int, @Query("limit") limit: Int, @Query("api_token") apiToken: String): Single<RemoteResponse<List<PersonData>>>

    @GET("v1/persons/{id}")
    fun getPersonById(@Path("id") id: Int, @Query("api_token") apiToken: String): Single<RemoteResponse<PersonData>>
}