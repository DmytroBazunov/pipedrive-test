package com.bazdmy.pipedrivetest.persons

import com.bazdmy.pipedrivetest.database.PersonDao
import com.bazdmy.pipedrivetest.database.PersonWithEmail
import com.bazdmy.pipedrivetest.entity.RemoteException
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PersonRepository @Inject constructor(private val dao: PersonDao,
                                           private val personApi: PersonsApi,
                                           private val apiKey: String) {


    fun getPersonById(personId: Int) =
            dao.getPersonInfoById(personId)


    fun getPersonsWithOffset(offset: Int, limit: Int, onError: (Pair<State, String>) -> Unit): Single<List<PersonWithEmail>> {
        return personApi.getAllPersons(offset, limit, apiKey)
                .doOnEvent { it, t ->
                    if (it != null && it.success != true) {
                        throw RemoteException("$it.errorInfo")
                    }
                    val data = it?.data ?: return@doOnEvent
                    dao.insertReceivedPersons(data)
                }
                .flatMap { dao.getPersons(offset, limit) }
                .onErrorResumeNext {
                    onError(State.ERROR to it.localizedMessage)
                    dao.getPersons(offset, limit)
                }

    }

    fun syncPerson(personId: Int): Completable = personApi.getPersonById(personId, apiKey)
            .subscribeOn(Schedulers.io())
            .doOnEvent { t1, t2 ->
                val data = t1?.data ?: return@doOnEvent
                dao.insertReceivedPerson(data)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .flatMapCompletable { Completable.complete() }

}