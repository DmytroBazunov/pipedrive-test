package com.bazdmy.pipedrivetest.persons


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

import com.bazdmy.pipedrivetest.R
import com.bazdmy.pipedrivetest.persondetail.PersonDetailViewModel
import com.bazdmy.pipedrivetest.persondetail.PersonDetailViewModelFactory
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_persons_list.*
import javax.inject.Inject


class PersonsListFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: PersonsViewModelFactory
    @Inject
    lateinit var viewModelDetailFactory: PersonDetailViewModelFactory

    private lateinit var viewModel: PersonsListViewModel
    private lateinit var viewModelDetail: PersonDetailViewModel

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        viewModel = ViewModelProviders.of(context as FragmentActivity, viewModelFactory)
                .get(PersonsListViewModel::class.java)
        viewModelDetail = ViewModelProviders.of(context as FragmentActivity, viewModelDetailFactory)
                .get(PersonDetailViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_persons_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()
    }

    private lateinit var personsListAdapter: PersonsListAdapter
    private fun initAdapter() {
        personsListAdapter = PersonsListAdapter() { it ->
            it?.let {
                viewModelDetail.setPersonId(it.person.id)
            }
        }
        persons_rv.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        persons_rv.adapter = personsListAdapter
        viewModel.personsList.observe(this, Observer {
            personsListAdapter.submitList(it)
        })
        viewModel.getState().observe(this, Observer {
            when (it?.first) {
                State.LOADING -> progressBar.visibility = View.VISIBLE
                State.ERROR -> {
                    Snackbar.make(progressBar, it.second, Snackbar.LENGTH_LONG).show()
                    progressBar.visibility = View.GONE
                }
                else -> progressBar.visibility = View.GONE
            }
        })


    }
}


