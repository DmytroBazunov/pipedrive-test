package com.bazdmy.pipedrivetest

import android.app.Activity
import android.app.Application
import com.bazdmy.pipedrivetest.di.DaggerApplicationComponent
import com.facebook.stetho.Stetho
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class App : Application(), HasActivityInjector {
    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector() = dispatchingActivityInjector

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        DaggerApplicationComponent
                .builder()
                .application(this)
                .build()
                .inject(this)
    }
}