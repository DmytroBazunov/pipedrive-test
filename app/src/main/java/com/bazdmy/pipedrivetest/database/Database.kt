package com.bazdmy.pipedrivetest.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.bazdmy.pipedrivetest.entity.PersonData
import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import java.io.UnsupportedEncodingException
import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


@Database(entities = [Person::class, Organization::class, Email::class, Phone::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun personDao(): PersonDao
}

@Dao
abstract class PersonDao {
//    @Query("SELECT * FROM person")
//    fun getAllPersonsDS(): DataSource.Factory<Integer, PersonData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertPerson(person: Person)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertPersons(persons: List<Person>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertOrganization(organization: Organization)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertEmails(email: List<Email>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertPhones(phone: List<Phone>)

    @Transaction
    @Query("SELECT * FROM person WHERE id = :id")
    abstract fun getPersonInfoById(id: Int): LiveData<PersonInfo>


    @Transaction
    @Query("SELECT * FROM person  LIMIT :limit OFFSET :offset")
    abstract fun getPersons(offset: Int, limit: Int): Single<List<PersonWithEmail>>

    @Transaction
    open fun insertReceivedPerson(person: PersonData) {


        person.organization?.let {
            insertOrganization(it)
            person.organization_id = it.value
        }
        insertPerson(person)
        person.email?.forEach { it.person_id = person.id }
        person.phone?.forEach { it.person_id = person.id }
        person.email?.let { insertEmails(it) }
        person.phone?.let { insertPhones(it) }

    }

    @Transaction
    open fun insertReceivedPersons(persons: List<PersonData>) {
        for (person in persons) {
            person.organization?.let {
                insertOrganization(it)
                person.organization_id = it.value
            }
            person.email?.forEach { it.person_id = person.id }
            person.phone?.forEach { it.person_id = person.id }
            person.email?.let { insertEmails(it) }
            person.phone?.let { insertPhones(it) }
        }

        insertPersons(persons)

    }

}

@Entity
open class Person(
        @SerializedName("id") @PrimaryKey var id: Int = 0,
        @SerializedName("company_id") var company_id: Int? = 0,
        var organization_id: Int = 0,
        @SerializedName("name") var name: String? = "",
        @SerializedName("first_name") var firstName: String? = "",
        @SerializedName("last_name") var lastName: String? = "",
        @SerializedName("label") var label: Int? = 0,
        @SerializedName("org_name") var orgName: String? = ""
)

@Entity
open class Organization(
        @SerializedName("name") var name: String? = "",
        @SerializedName("people_count") var peopleCount: Int? = 0,
        @SerializedName("owner_id") var ownerId: Int? = 0,
        @SerializedName("address") var address: String? = "",
        @SerializedName("cc_email") var ccEmail: String? = "",
        @SerializedName("value") @PrimaryKey var value: Int = 0
)


@Entity(primaryKeys = ["value", "person_id"])
open class Email(
        @SerializedName("label") var label: String? = "",
        @SerializedName("value") var value: String = "",
        @SerializedName("primary") var primary: Boolean? = false,
        var person_id: Int = 0
)

@Entity(primaryKeys = ["value", "person_id"])
open class Phone(
        @SerializedName("label") var label: String? = "",
        @SerializedName("value") var value: String = "",
        @SerializedName("primary") var primary: Boolean? = false,
        var person_id: Int = 0
)

open class PersonWithEmail {
    @Embedded
    lateinit var person: Person
    @Relation(parentColumn = "id", entityColumn = "person_id", entity = Email::class)
    var emails: List<Email> = listOf()


    fun getGravatarURL(width: Int): String {
        val messageDigest: MessageDigest
        val primaryEmail = emails.find { it.primary ?: false }?.value
                ?: return "https://www.gravatar.com/avatar/00000000000000000000000000000000?s=$width"
        return try {
            messageDigest = MessageDigest.getInstance("MD5")
            messageDigest.reset()
            "https://www.gravatar.com/avatar/${bin2hex(messageDigest.digest(primaryEmail.toByteArray(charset("UTF-8"))))}?s=$width"
        } catch (e: NoSuchAlgorithmException) {
            "https://www.gravatar.com/avatar/00000000000000000000000000000000?s=$width"
        } catch (e: UnsupportedEncodingException) {
            "https://www.gravatar.com/avatar/00000000000000000000000000000000?s=$width"
        }
    }

    private fun bin2hex(data: ByteArray): String {
        return String.format("%0" + data.size * 2 + 'x'.toString(), BigInteger(1, data))
    }

}

class PersonInfo : PersonWithEmail() {


    @Relation(parentColumn = "organization_id", entityColumn = "value", entity = Organization::class)
    var backingOrg: List<Organization> = listOf()
    @Relation(parentColumn = "id", entityColumn = "person_id", entity = Phone::class)
    lateinit var phones: List<Phone>
    val organization: Organization
        get() = backingOrg.first()
}

